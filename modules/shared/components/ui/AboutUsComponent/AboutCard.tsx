import React from 'react';
import { AboutCardWrapper, CardInfo } from './views';

interface AboutCardProps {
  title: string;
  image: string;
}

const AboutCard: React.FC<AboutCardProps> = ({ title, image }) => {
  return (
    <AboutCardWrapper>
      <img src={`/static/images/${image}.png`} alt='web-dev' />
      <CardInfo>
        <div>{title}</div>
        <div>Lorem ipsum dolor sit amet consectetur adipisicing.</div>
      </CardInfo>
    </AboutCardWrapper>
  );
};

export { AboutCard };
