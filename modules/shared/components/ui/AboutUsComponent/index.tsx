import React from 'react';
import Button from '../Button';
import { LinkToProjectWrapper } from '../LinkToProject/views';
import { Title } from '../Slider/views';
import { AboutCard } from './AboutCard';
import { AboutUsComponentWrapper, Content, Grid, LapTopI, Image, LinesI, NodeI, ReactI, RubyI } from './views';

const AboutUsComponent = () => {
  return (
    <AboutUsComponentWrapper>
      <Content>
        <LapTopI>
          <Image src={'/static/images/devices.png'} />
        </LapTopI>
        <LinesI>
          <Image src={'/static/images/lines-copy.png'} />
        </LinesI>
        <NodeI>
          <Image src={'/static/images/nodejs-1.png'} />
        </NodeI>
        <ReactI>
          <Image src={'/static/images/ruby.png'} />
        </ReactI>
        <RubyI>
          <Image src={'/static/images/react.png'} />
        </RubyI>

        <div>
          <LinkToProjectWrapper display='flex'>
            <Title>There are a couple of things we do really well.</Title>
            <Button />
          </LinkToProjectWrapper>

          <Grid>
            <AboutCard image={'web-dev'} title={'Web Dev'} />
            <AboutCard image={'mobile'} title={'Mobile'} />
            <AboutCard image={'e-commerce'} title={'E-Commerce'} />
            <AboutCard image={'saas'} title={'SAAS'} />
          </Grid>
        </div>
      </Content>
    </AboutUsComponentWrapper>
  );
};

export { AboutUsComponent };
