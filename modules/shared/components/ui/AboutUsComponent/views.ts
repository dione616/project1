import styled from 'styled-components';

export const AboutUsComponentWrapper = styled.div`
  background-color: white;
  position: relative;
  z-index: 5;
  padding-top: 80px;
  backface-visibility: hidden;
`;
export const Content = styled.div`
  position: relative;
  padding: 100px 10vw 50px 450px;
  display: flex;
  justify-content: space-around;
  z-index: 5;
  background: rgb(255, 255, 255);
`;
export const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
`;
export const AboutCardWrapper = styled.div`
  display: flex;
  padding: 50px 45px 50px 0;
  max-width: 500px;

  font-size: 22px;
  line-height: 22px;
  opacity: 0.6;

  & > img {
    height: 30%;
    margin-right: 20px;
  }
`;
export const CardInfo = styled.div`
  color: ${({ theme }) => theme.colors.gray};
`;
export const LapTopI = styled.div`
  position: absolute;
  top: 0px;
  left: 0px;
  width: 380px;
  z-index: 1;
`;
export const LinesI = styled.div`
  position: absolute;
  top: 110px;
  left: 0px;
  width: 320px;
`;
export const NodeI = styled.div`
  position: absolute;

  width: 150px;
  height: 100px;
  top: 115px;
  left: 150px;

  display: flex;
  align-items: center;
  justify-content: center;

  border-radius: 10px;

  background-color: white;
  z-index: 2;
`;
export const ReactI = styled.div`
  position: absolute;

  width: 80px;
  height: 80px;
  top: 60px;
  left: 250px;

  display: flex;
  align-items: center;
  justify-content: center;

  border-radius: 10px;

  background: #e5f9ff;
  z-index: 4;
`;
export const RubyI = styled.div`
  position: absolute;

  width: 80px;
  height: 60px;
  top: 110px;
  left: 310px;

  display: flex;
  align-items: center;
  justify-content: center;

  border-radius: 10px;

  background: #ffe2e2;
  z-index: 3;
`;
export const Image = styled.img.attrs((props) => ({
  src: props.src
}))``;

export const GoToWrapper = styled.div`
  display: flex;
`;
