import React from 'react';
import { GradientButtonWrapper } from './views';

interface Props {
  children: string;
}

const GradientButton: React.FC<Props> = ({ children }) => {
  return <GradientButtonWrapper>{children}</GradientButtonWrapper>;
};

export { GradientButton };
