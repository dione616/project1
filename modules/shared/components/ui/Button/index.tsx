import React from 'react';
import { ButtonWrapper, Image } from './views';

const Button = () => {
  return (
    <ButtonWrapper>
      <Image src='/static/images/shape@2x.png' />
      View project
    </ButtonWrapper>
  );
};

export default Button;
