import styled from 'styled-components';

export const ButtonWrapper = styled.div`
  display: flex;
  max-width: 100px;
  align-items: center;
  margin-top: 40px;
  cursor: pointer;
  text-transform: uppercase;
  color: #fa7d75;
  justify-content: space-between;

  & > img {
    margin-right: 30px;
  }
`;

export const GradientButtonWrapper = styled.button`
  background: linear-gradient(45deg, #fd5e5f, #fe9876);
  text-transform: uppercase;
  padding: 15px 25px;
  letter-spaceing: 1.5;
  border: none;
  border-radius: 30px;
  cursor: pointer;
  z-index: 5;
  letter-spacing: 2px;
  font-weight: 500;
  color: white;

  border: none;
  outline: none;

  &:hover {
    background: linear-gradient(45deg, #d24f4f, #da8467);
  }
`;

export const Image = styled.img.attrs((props) => ({
  src: props.src
}))``;
