import React from 'react';
import { Image } from '../AboutUsComponent/views';
import { GradientButton } from '../Button/GradientButton';
import { FooterWrapper, FooterSection, ImgWrapper, Rights, Title, Info } from './views';

const Footer = () => {
  return (
    <FooterWrapper>
      <FooterSection>
        <Image src={'/static/images/astronaut.png'} />
        <ImgWrapper>
          <Info>
            <Title>We' re just as excited about your idea.</Title>
            <GradientButton>Tell us more</GradientButton>
          </Info>
          <Rights>2018 Cultum LLC. All rights reserved.</Rights>
        </ImgWrapper>
      </FooterSection>
    </FooterWrapper>
  );
};

export { Footer };
