import styled from 'styled-components';

export const FooterWrapper = styled.div`
  position: relative;
  height: 100vh;
  z-index: 5;
`;
export const FooterSection = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  backface-visibility: hidden;

  height: 100vh;
  width: 100vw;
  left: 0px;
  right: 0px;
  bottom: 0px;

  background: white;
  overflow: hidden;

  padding-top: 300px;

  & > img {
    position: absolute;
    z-index: 6;
    top: 150px;
    transform: translateX(calc(30%));
    width: 200px;
  }
`;

export const ImgWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;

  width: 100%;
  height: 100%;
  padding: 150px 30px 10px;

  color: white;

  &:before {
    content: '';
    position: absolute;
    top: 0px;
    bottom: 0px;
    left: 0px;
    right: 0px;
    background: url(/static/images/moon.png) center top / cover no-repeat;
    z-index: 5;
  }
`;

export const Title = styled.h1`
  text-align: center;
  z-index: 5;
  max-width: 450px;
`;
export const Info = styled.div`
  padding: 100px;
  z-index: 5;
  text-align: center;
`;
export const Rights = styled.div`
  text-align: center;
  z-index: 5;
  font-size: 1.25rem;
  position: absolute;
  bottom: 20px;
`;
