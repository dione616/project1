import React from 'react';
import { Illustration, ImageWrapper, IllustrationTop } from './views';

interface Props {
  x: number;
  y: number;
}

const Image: React.FC<Props> = ({ x, y }) => {
  return (
    <ImageWrapper>
      <IllustrationTop>
        <img
          style={{ transform: `translateX(${(x - x - x) / 50}px) translateY(${(y - y - y) / 20}px)` }}
          src='/static/images/top-planet.svg'
          alt='illustration'
        />
      </IllustrationTop>

      <Illustration>
        <img src='/static/images/cultum.svg' alt='illustration' />
      </Illustration>

      <img
        style={{ transform: `translateX(${(x - x - x) / 10}px) translateY(${(y - y - y) / 5}px)` }}
        src='/static/images/bot-planet.svg'
        alt='illustration'
      />
    </ImageWrapper>
  );
};

export { Image };
