import styled from 'styled-components';

export const ImageWrapper = styled.div`
  position: relative;
  max-width: 600px;
  width: 90%;
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  -webkit-box-pack: center;
  justify-content: center;
  max-height: calc(100vh - 60px);

  @media (max-width: 1200px) {
    margin-top: 300px;
  }
`;
export const Illustration = styled.div`
  img {
    height: 500px;
  }
`;
export const IllustrationTop = styled.div`
  img {
    position: absolute;
    top: calc(10%+10px);
    top: 0px;
    left: 0px;
  }
`;
