import React from 'react';
import { LinkToProjectWrapper } from '../LinkToProject/views';
import { CardProps } from './LettersComponent';
import { Title, Description, TechDescription, ButtonWrapper } from './views';

const Card: React.FC<CardProps> = ({ title, desc, tech }) => {
  return (
    <LinkToProjectWrapper>
      <Title>{title}</Title>
      <Description>{desc}</Description>
      <TechDescription>{tech}</TechDescription>
      <ButtonWrapper>
        <img src='/static/images/shape@2x.png' alt='' />
        View project
      </ButtonWrapper>
    </LinkToProjectWrapper>
  );
};

export { Card };
