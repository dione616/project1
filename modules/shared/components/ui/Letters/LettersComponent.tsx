import React from 'react';
import { Single } from './Single';
import { LettersComponentWrapper } from './views';

export interface CardProps {
  title?: string;
  desc?: string;
  tech: string;
  image?: string;
}

const LettersComponent: React.FC<CardProps> = ({ title, desc, tech, image }) => {
  return (
    <LettersComponentWrapper>
      <Single title={title} desc={desc} tech={tech} image={image} />
    </LettersComponentWrapper>
  );
};

export { LettersComponent };
