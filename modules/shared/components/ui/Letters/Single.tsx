import React from 'react';
import { CardProps } from './LettersComponent';
import { CardWrapper } from './views';
import { Card } from './Card';

const Single: React.FC<CardProps> = ({ title, desc, tech, image }) => {
  return (
    <CardWrapper>
      <Card title={title} desc={desc} tech={tech} />
      <img src={`/static/images/picture-${image}.png`} alt='' />
    </CardWrapper>
  );
};

export { Single };
