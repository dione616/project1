import React, { useState } from 'react';
import { ImgWrapper, LettersWrapper, ImgNav, SliderImage, WrapperWithId, ParentContainer } from './views';
import { LettersComponent } from './LettersComponent';
import { Image } from '../AboutUsComponent/views';

const LettersSection = () => {
  const [coord, setCoord] = useState(36);
  const navTo = (newCoord: number) => {
    setCoord(newCoord);
  };
  return (
    <LettersWrapper>
      <ParentContainer>
        <ImgWrapper>
          <Image src='/static/images/earth.png' />
          <ImgNav href='#section1' onClick={() => navTo(36)} rotate={36} />
          <ImgNav href='#section2' onClick={() => navTo(72)} rotate={72} />
          <ImgNav href='#section3' onClick={() => navTo(118)} rotate={118} />
          <ImgNav onClick={() => navTo(144)} rotate={144} />
          <SliderImage coord={coord}>
            <Image src='/static/images/slider.png' />
          </SliderImage>
        </ImgWrapper>

        <WrapperWithId id='section1'>
          <LettersComponent
            image='1'
            title={'Rentloop'}
            desc={'Evolving and expanding aggregation of financial loans.'}
            tech='Web development - GraphQL and React.js'
          />
        </WrapperWithId>

        <WrapperWithId id='section2'>
          <LettersComponent
            image='2'
            title={'Scin Carisma'}
            desc={'Evolving and expanding aggregation of financial loans.'}
            tech='Web development - GraphQL and React.js'
          />
        </WrapperWithId>

        <WrapperWithId id='section3'>
          <LettersComponent
            image='3'
            title={'Creadit Script'}
            desc={'Evolving and expanding aggregation of financial loans.'}
            tech='Web development - GraphQL and React.js'
          />
        </WrapperWithId>
      </ParentContainer>
    </LettersWrapper>
  );
};

export { LettersSection };
