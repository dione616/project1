import styled from 'styled-components';

export const LettersWrapper = styled.div`
  position: relative;
  z-index: 3;
  background: linear-gradient(to right, #371a3a, #7a3547);
  backface-visibility: hidden;

  &:before {
    content: '';
    position: absolute;
    top: 0px;
    bottom: 0px;
    left: 0px;
    right: 0px;
    transition: opacity 0.4s linear 0s;
    background: linear-gradient(to right, #371a3a, #7a3547);
    opacity: 0;
    z-index: 0;
  }
`;

export const ParentContainer = styled.div`
  position: relative;
`;
export const LettersComponentWrapper = styled.div`
  position: relative;
  min-height: 100vh;
  display: flex;
  -webkit-box-align: center;
  align-items: center;
`;

interface Props {
  rotate: number;
}

export const ImgNav = styled.a<Props>`
  position: absolute;
  top: 0px;
  left: 50%;
  width: 11px;
  height: 11px;

  border-radius: 50%;
  background-image: linear-gradient(150deg, #fe8081, #ffcead);

  transform-origin: center calc((32.5vh + 5.5px) - 1px);
  transform: translate3d(-50%, -50%, 0px) rotateZ(${({ rotate }) => rotate}deg);

  cursor: pointer;
`;
interface SliderProps {
  coord: number;
}
export const SliderImage = styled.div<SliderProps>`
  position: absolute;
  top: 0px;
  left: 50%;
  height: 57px;
  width: 57px;
  transform-origin: center calc(32.5vh + 28.5px);
  z-index: 6;
  transform: translate3d(-50%, -50%, 0px) rotateZ(${({ coord }) => coord}deg);
  transition: transform 0.6s cubic-bezier(0.5, 0.05, 0.36, 1) 0s;
`;

export const ImgWrapper = styled.div`
  position: fixed;
  z-index: 1;
  right: 100%;
  top: 90.825px;
  min-width: 65vh;
  min-height: 65vh;
  transform: translateX(190px);
  border: 1px solid rgba(255, 255, 255, 0.4);
  border-radius: 50%;

  & > img {
    position: absolute;
    right: 190px;
    top: 50%;
    transform: translateY(-50%) translateX(100%);
  }
`;

export const Title = styled.h1`
  color: ${({ theme }) => theme.colors.white};
  font-size: 2.4rem;
  font-family: 'Raleway', sans-serif;
  font-weight: 900;
`;
export const Description = styled.div`
  color: ${({ theme }) => theme.colors.white};
  font-size: 1.3rem;
  opacity: 0.6;
  line-height: 1.5;
  font-weight: 400;
`;
export const TechDescription = styled.div`
  color: ${({ theme }) => theme.colors.gray};
  font-size: 1.3rem;
  opacity: 0.6;
  line-height: 1.5;
  font-weight: 400;
`;
export const Button = styled.button`
  background: linear-gradient(45deg, #fd5e5f, #fe9876);
  text-transform: uppercase;
  padding: 15px 25px;

  margin-top: 40px;
  border-radius: 30px;
  &:hover {
    background: linear-gradient(45deg, #d24f4f, #da8467);
  }
`;
export const ButtonWrapper = styled.div`
  display: flex;
  width: 40%;
  align-items: center;
  margin-top: 40px;
  cursor: pointer;
  text-transform: uppercase;
  color: #fa7d75;
  justify-content: space-between;
`;

export const CardWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  padding: 30px 70px 30px 100px;
  left: 0px;
  top: 0px;
  right: 0px;
  width: 100vw;

  & > div {
    max-width: 500px;
  }
`;
export const WrapperWithId = styled.div``;

//    background: linear-gradient(to right, rgb(55, 26, 58) , rgb(122, 53, 71));
//    background: linear-gradient(to right, rgb(125, 28, 47) , rgb(183, 68, 100));
