import styled from 'styled-components';

interface Props {
  display?: string;
}

export const LinkToProjectWrapper = styled.div<Props>`
  display: ${({ display }) => (display ? display : 'block')};
`;
