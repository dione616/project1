import React, { MouseEvent, useState } from 'react';
import { Image } from '../Image';
import { MainLeft } from './MainLeft';
import { MainWrapper, MainCard } from './views';

const Main = () => {
  const [coord, setCoord] = useState({ x: 0, y: 0 });

  const moveEvent = (event: MouseEvent) => {
    const x = event.clientX;
    const y = event.clientY;
    setCoord({ x, y });
  };

  return (
    <MainWrapper onMouseMove={(event) => moveEvent(event)}>
      <MainCard>
        <MainLeft />
        <Image x={coord.x} y={coord.y} />
      </MainCard>
    </MainWrapper>
  );
};

export { Main };
