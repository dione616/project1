import React from 'react';
import { GradientButtonWrapper } from '../Button/views';
import { MainLeftWrapper, Title, Description, ButtonsSection } from './views';

const MainLeft = () => {
  return (
    <MainLeftWrapper>
      <Title>Digital experiences that make you shine.</Title>
      <Description>Cultum is a digital agency where strategy, creativity and technology converge.</Description>
      <ButtonsSection>
        <GradientButtonWrapper>drop us a line</GradientButtonWrapper>
      </ButtonsSection>
    </MainLeftWrapper>
  );
};

export { MainLeft };
