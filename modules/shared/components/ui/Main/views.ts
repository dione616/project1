import styled from 'styled-components';

export const MainWrapper = styled.div`
  height: 100vh;
  position: relative;
  z-index: 0;
`;
export const MainCard = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;

  position: fixed;

  height: 100vh;

  padding: 30px 70px 30px 100px;
  left: 0px;
  top: 0px;
  right: 0px;

  &:before {
    content: '';
    position: absolute;
    width: 250px;
    height: 300%;
    top: 50%;
    right: 20%;
    background-color: #fff7f7;
    transform: translateY(-50%) rotateZ(-45deg);
  }
`;

export const MainLeftWrapper = styled.div`
  color: ${({ theme }) => theme.colors.gray};
  max-width: 500px;
  padding-right: 70px;
  position: relative;
`;
export const Title = styled.h1`
  color: ${({ theme }) => theme.colors.gray};
  font-size: 2.4rem;
  font-family: 'Raleway', sans-serif;
  font-weight: 900;
`;
export const Description = styled.div`
  color: ${({ theme }) => theme.colors.gray};
  font-size: 1.3rem;
  opacity: 0.6;
  line-height: 1.5;
  font-weight: 400;
`;
export const ButtonsSection = styled.div`
  padding-top: 80px;
`;
export const Button = styled.button`
  background: linear-gradient(45deg, #fd5e5f, #fe9876);
  text-transform: uppercase;
  padding: 15px 25px;
  letter-spaceing: 1.5;
  position: absolute;
  top: calc(100% + 80px);
  border: none;
  border-radius: 30px;
  cursor: pointer;

  &:hover {
    background: linear-gradient(45deg, #d24f4f, #da8467);
  }
`;
