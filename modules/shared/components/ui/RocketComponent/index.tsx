import React from 'react';
import { Single } from '../Letters/Single';
import { RocketComponentWrapper } from './views';

const RocketComponent = () => {
  return (
    <RocketComponentWrapper>
      <Single
        image='4'
        tech={
          'Oh by the way, we are Cultum. A passionate, tight-knit tema of premier specialists who can take on any challange in the sphere of web and mobile app development.'
        }
      />
    </RocketComponentWrapper>
  );
};

export { RocketComponent };
