import styled from 'styled-components';
export const RocketComponentWrapper = styled.div`
  background-color: white;
  position: relative;
  z-index: 5;
  backface-visibility: hidden;

  & > div > img {
    height: 600px;
  }
`;
