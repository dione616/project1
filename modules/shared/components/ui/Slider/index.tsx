import React, { Component } from 'react';
import Slider from 'react-slick';
import { Image } from '../AboutUsComponent/views';
import { CardWrapper } from '../Letters/views';
import { SliderWrapper, Review, Title, ReviewWrapper, Author } from './views';

function SampleNextArrow(props: Props) {
  const { className, onClick } = props;
  return (
    <Image
      onClick={onClick}
      className={className}
      style={{ width: '50px', top: 'unset', zIndex: 5, bottom: '30px' }}
      src={'/static/images/shape.png'}
    />
  );
}

interface Props {
  className?: string;
  onClick?: () => void;
}

function SamplePrevArrow(props: Props) {
  const { className, onClick } = props;
  return (
    <Image
      onClick={onClick}
      className={className}
      style={{ transform: 'rotateZ(180deg)', zIndex: 5, width: '50px', top: 'unset', right: '30px' }}
      src={'/static/images/shape.png'}
    />
  );
}

export class CustomArrows extends Component {
  render() {
    const settings = {
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />
    };
    return (
      <SliderWrapper>
        <CardWrapper>
          <Title>Seems like our clients love us...</Title>
          <Slider {...settings}>
            <ReviewWrapper>
              <Review>
                We ask a lot of these guys and are continually throwing new stuff at them-the fact that they just do the
                woek quickly with no fuss is a massive help.
              </Review>
              <Author>
                <Image src={'/static/images/avatar.jpg'} />
                <div>Woman Blond, ceo huge</div>
              </Author>
            </ReviewWrapper>
            <ReviewWrapper>
              <Review>
                We ask a lot of these guys and are continually throwing new stuff at them-the fact that they just do the
                woek quickly with no fuss is a massive help.
              </Review>
              <Author>
                <Image src={'/static/images/avatar2.png'} />
                <div>john smith, ceo huge</div>
              </Author>
            </ReviewWrapper>
            <ReviewWrapper>
              <Review>
                We ask a lot of these guys and are continually throwing new stuff at them-the fact that they just do the
                woek quickly with no fuss is a massive help.
              </Review>
              <Author>
                <Image src={'/static/images/avatar.jpg'} />
                <div>aaron shapir, ceo huge</div>
              </Author>
            </ReviewWrapper>
          </Slider>
        </CardWrapper>
      </SliderWrapper>
    );
  }
}
