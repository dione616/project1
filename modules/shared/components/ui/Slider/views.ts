import styled from 'styled-components';

export const SliderWrapper = styled.div`
  backface-visibility: hidden;
  z-index: 5;
  background: white;
  position: relative;
  display: flex;

  & .slick-prev {
    right: 30px;
    left: unset;
    bottom: 40px;
  }
`;
export const Title = styled.h1`
  width: 430px;
`;
export const Review = styled.h2`
  margin: 25px;
  position: relative;

  &:before {
    content: '“';
    position: absolute;
    font-size: 40px;
    line-height: 15px;
    opacity: 0.5;
    left: -25px;
  }

  &:after {
    content: '“';
    position: absolute;
    font-size: 40px;
    line-height: 15px;
    opacity: 0.5;
    right: -25px;
  }
`;
export const ReviewWrapper = styled.div`
  max-width: 600px;
`;
export const Author = styled.div`
  display: flex;
  align-items: center;

  & > img {
    border-radius: 50%;
    height: 80px;
  }
  & > div {
    color: #ef797a;
    text-transform: uppercase;
  }
`;
