import * as React from 'react';

import { Logo } from '@md-ui/logos/main';

// views
import { HeaderWrapper, LWrapper, RWrapper } from './views';
import { Menu } from '../../menu';

const Header = () => {
  return (
    <HeaderWrapper>
      <LWrapper>
        <Logo />
      </LWrapper>
      <RWrapper>
        <Menu />
      </RWrapper>
    </HeaderWrapper>
  );
};

export { Header };
