import styled from 'styled-components';

export const HeaderWrapper = styled.div`
  position: fixed;
  top: 0px;
  left: 0px;
  right: 0px;
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  -webkit-box-pack: justify;
  justify-content: space-between;
  z-index: 10000;
  transition: all 0.3s ease 0s;
  backface-visibility: hidden;
  transform: translate3d(0px, 0px, 0px);
  padding-left: 100px;
  padding-right: 70px;
  padding-top: 35px;
`;

export const IWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 5px 10px;
  margin: 0 auto;

  width: 100vw;
`;

export const LWrapper = styled.div`
  flex: 1;

  img {
    display: block;
  }

  ${({ theme }) => theme.templates.centerItems};
`;

export const RWrapper = styled.div`
  flex: 2;
  justify-content: flex-end;

  ${({ theme }) => theme.templates.centerItems};
`;
