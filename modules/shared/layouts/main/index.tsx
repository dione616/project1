import * as React from 'react';
// view components
import { Header } from '@md-ui/headers/main';
// views
import { Wrapper, PageWrapper } from './views';

const MainLayout: React.FC = ({ children }) => (
  <Wrapper>
    <PageWrapper>
      <Header />
      {children}
    </PageWrapper>
  </Wrapper>
);

export { MainLayout };
