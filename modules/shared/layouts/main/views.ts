import styled from 'styled-components';

export const Wrapper = styled.div`
  position: relative;
  height: auto;
  @media (max-width: 1200px) {
    margin-top: 300px;
  }
`;
export const PageWrapper = styled.div`
  position: relative;
  overflow-x: hidden;
`;
