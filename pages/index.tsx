import * as React from 'react';
import { MainLayout } from '@md-modules/shared/layouts/main';
import { Main } from '@md-modules/shared/components/ui/Main/Main';
import { LettersSection } from '@md-modules/shared/components/ui/Letters';
import { RocketComponent } from '@md-modules/shared/components/ui/RocketComponent';
import { AboutUsComponent } from '@md-modules/shared/components/ui/AboutUsComponent';
import { CustomArrows } from '@md-modules/shared/components/ui/Slider/index.tsx';
import { Footer } from '@md-modules/shared/components/ui/Footer';

const Home = () => {
  return (
    <MainLayout>
      <Main />
      <LettersSection />
      <RocketComponent />
      <AboutUsComponent />
      <CustomArrows />
      <Footer />
    </MainLayout>
  );
};

export default Home;
